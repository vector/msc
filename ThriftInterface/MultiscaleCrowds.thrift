
namespace cpp imi
namespace java imi
namespace php imi

// Common Data Structures 
typedef i32 agent_id

struct AgentPosition { 
	1: double x;
	2: double y;
	4: double vx;  
	5: double vy;  
}

struct FlowElement {
	1: double x;
	2: double y;
	3: double dx;
	4: double dy;
}

//////////////////////////////////////////////////////////////////////////////
// Animated Characters are 3D characters with locomotion (and possibly other)
// animations.
service AnimatedCharacterRendering {

	bool createWorld(/* TODO */);
	
	// Draw of all agents for the current frame, and wait for acknowledgement
	// Returns true if all agents could be drawn successfully
	bool drawAgents( 1: double aTime,
					 2: list< AnimatedAgentPositionVec > aAgentStates );

	// Same as drawFrame(), but the client is not blocked waiting for return,
	// however, the client has no guarantee that the
	oneway void drawAgentsASync( 1: double aTime,
								 2: list< AnimatedAgentPositionVec > aAgentStates );

	// 
	//oneway void beginFrame( 1: double aTime );
	//oneway void endFrame();
	//oneway void drawAgentInFrame( AnimatedAgentPositionVec aAgentState );
    //
}


// Rendering service for articulated bodies
service ArticulatedBodyRendering {

	// Initialize world, loading background and context geometry as needed.
	bool createWorld( 1: string argName );
	
	// Start the next frame.  Must be called at least once before rendering.
	// Note that for real-time or interactive rendering, explicit frames may be ignored by the renderer.
	// endFrame() is optional.  To advance to the next frame, call beginFrame again with a later time.
	i32 beginFrame( 1: double argTime );
	
	// Draw a box of the given dimensions, centered at the origin after applying the AffineTransformation.
	oneway void drawBox( 1: AffineTransform xform, 2: double xsize, 3: double ysize, 4: double zsize );
	
	// drawTri
	// drawCapsule
	
	// Draw a line segment 
	oneway void drawLineSegment( 1: Vec3 argStart, 2: Vec3 argEnd );
	
	oneway void drawVector( 1: Vec3 argOrigin, 2: Vec3 argOffset );
}
