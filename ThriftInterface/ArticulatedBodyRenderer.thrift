///
/// @file ArticulatedBodyRenderer.hpp
/// Defines the interface for rendering 3D characters composed
/// of multiple, independently articulated bodies, such as driven by physics.
///
//------------------------------------------------------------------------------
// Copyright 2011 Brian F. Allen (vector@acm.org)
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//------------------------------------------------------------------------------

include "MultiscaleCrowdsCommon.thrift"

namespace cpp imi
namespace java imi
namespace php imi

//////////////////////////////////////////////////////////////////////////////
// Animated Characters are 3D characters with locomotion (and possibly other)
// animations.
service ArticulatedBodyRenderer {

	// Initialize world, loading background and context geometry as needed.
	bool createWorld( 1: string aName );

	// Start the next frame.  Must be called at least once before rendering.
	// Note that for real-time or interactive rendering, explicit frames may be ignored by the renderer.
	// endFrame() is optional.  To advance to the next frame, call beginFrame again with a later time.
	i32 beginFrame( 1: double aTime );

	bool endFrame();
	
	// Draw a box of the given dimensions, centered at the origin after applying the AffineTransformation.
	oneway void drawBox( 1: AffineTransform aXform, 2: double aXsize, 3: double aYsize, 4: double aZsize );
	
	// drawTri
	// drawCapsule
	
	// Draw a line segment 
	oneway void drawLineSegment( 1: Vec3 aStart, 2: Vec3 aEnd );

	// Draw a vector (with arrow head)
	oneway void drawVector( 1: Vec3 aOrigin, 2: Vec3 aOffset );
}
