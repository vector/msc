///
/// @file ParticleRenderer.hpp
/// Defines the interface for rendering particles and fields on a 2d plane.
///
//------------------------------------------------------------------------------
// Copyright 2011 Brian F. Allen (vector@acm.org)
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//------------------------------------------------------------------------------

include "MultiscaleCrowdsCommon.thrift"

namespace cpp imi
namespace java imi
namespace php imi

//////////////////////////////////////////////////////////////////////////////
// Particles are representative of the flows 
service ParticleRenderer {

	// Initialize the world and load the specified map.
	// If the specified map is found and correctly loaded, and if the given region is fully
	// contained inside the map, then true is returned.	 (Not throwing an exception here, as this
	// could be used to iterate over a set of map names to find an available one.)
	// createWorld will try to use a default/fallback map if argMapName is the empty string. 
	bool createWorld( 1: string aMapName, 
	                  2: double aMinX, 
	                  3: double aMinY, 
	                  4: double aMaxX, 
	                  5: double aMaxY );
	
	// Returns the name of the currently loaded map.  If createWorld has never been called
	// successfully, (i.e., it never returned true), the empty string will be returned.
	string currentMapName();

	// Ask to display a single frame using the specified agents and flow elements.
	// Note that these are not persistent at all, the next frame's draw must specify all
	// of the drawn positions and flow elements again.
	void draw( 1: double aTime, 
			   2: list< MultiscaleCrowdsCommon.AgentPos > aAgentStates, 
			   3: list< MultiscaleCrowdsCommon.FlowElement > aFlowElements );
}


