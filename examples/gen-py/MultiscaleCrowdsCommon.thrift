///
/// @file MultiscaleCrowdsCommon.thrift
/// Defines the common data structures and interfaces for 
/// the Multiscale crowd project.
///
//------------------------------------------------------------------------------
// Copyright 2011 Brian F. Allen (vector@acm.org)
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//------------------------------------------------------------------------------

namespace cpp imi
namespace java imi
namespace php imi

// Common Data Structures 

typedef i32 agent_id

// The 2D state of the 
struct AgentPos { 
    1: double x;
    2: double y;
    4: double vx;  
    5: double vy;  
}

struct FlowElement {
    1: double x;
    2: double y;
    3: double dx;
    4: double dy;
}

// The 2D state of a particular agent.
// Velocity (vx,vy) is used to determine the speed and direction of the 
// locomotion animation playback.
struct AnimatedAgentPos { 
    1: agent_id agentId;  //< unique ID of the agent, returned by a call to create...()
    2: AgentPos state;    //< Velocity (vx,vy) is used to determine the speed and direction of the 
                               //< locomotion animation playback.
}

// y = Rx
struct RotationMatrix {
    1: double r00;
    2: double r01;
    3: double r02;
    4: double r10;
    5: double r11;
    6: double r12;
    7: double r20;
    8: double r21;
    9: double r22;
}

// y = Rx + t
struct AffineTransform {
    1: double tX;
    2: double tY;
    3: double tZ;
    4: RotationMatrix rot;
}

struct Vec3 {
    1: double x;
    2: double y;
    3: double z;
}
