import sys
sys.path.append('gen-py')

from AnimatedAgentRenderer import *
from MultiscaleCrowdsCommon.ttypes import *

from thrift.transport import TTransport
from thrift.transport import TSocket
from thrift.protocol import TBinaryProtocol
from thrift.protocol import TCompactProtocol

#BUFFER Transport

p = AgentPos()
p.x = 1.
p.y = 1.
p.vx = 0.
p.vy = 0.01

ap = AnimatedAgentPos()
ap.agentId = 2
ap.state = p

p2 = AgentPos()
p2.x = 0.
p2.y = 1.
p2.vx = 0.01
p2.vy = 0.

ap2 = AnimatedAgentPos()
ap2.agentId = 3
ap2.state = p2


socket = TSocket.TSocket("localhost", 9090)

# Windows example uses FramedTransport
#transport = TTransport.TFramedTransport(socket)

# Default UNIX client uses buffered
transport = TTransport.TBufferedTransport(socket)

protocol = TBinaryProtocol.TBinaryProtocolFactory().getProtocol(transport)
transport.open()

aar = AnimatedAgentRenderer.Client(protocol)
aar.createWorld("Caprica", 0, 0, 10, 10)
print "Got new world: " + aar.currentMapName()

aar.drawAgents( 10000.0, [ap,ap2] )

transport.close()
