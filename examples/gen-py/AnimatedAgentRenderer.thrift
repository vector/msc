///
/// @file AnimatedAgentRenderer.thrift
/// Defines the interface for rendering animated characters 
/// based on 2d movement.
///
//------------------------------------------------------------------------------
// Copyright 2011 Brian F. Allen (vector@acm.org)
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//------------------------------------------------------------------------------

include "MultiscaleCrowdsCommon.thrift"

namespace cpp imi
namespace java imi
namespace php imi

//////////////////////////////////////////////////////////////////////////////
// Animated Characters are 3D characters with locomotion (and possibly other)
// animations.
service AnimatedAgentRenderer {

	// Initialize the world and load the specified map.
	// If the specified map is found and correctly loaded, and if the given region is fully
	// contained inside the map, then true is returned.	 (Not throwing an exception here, as this
	// could be used to iterate over a set of map names to find an available one.)
	// createWorld will try to use a default/fallback map if argMapName is the empty string. 
	bool createWorld( 1: string aMapName, 
	                  2: double aMinX, 
	                  3: double aMinY, 
	                  4: double aMaxX, 
	                  5: double aMaxY );

	// Returns the name of the currently loaded map.  If createWorld has never been called
	// successfully, (i.e., it never returned true), the empty string will be returned.
	string currentMapName();

	// Draw of all agents for the current frame, and wait for acknowledgement
	// Returns true if all agents could be drawn successfully
	bool drawAgents( 1: double aTime,
					 2: list< MultiscaleCrowdsCommon.AnimatedAgentPos > aAgentStates );


	// Same as drawFrame(), but the client is not blocked waiting for return,
	// however, the client has no guarantee that the
	oneway void drawAgentsASync( 1: double aTime,
								 2: list< MultiscaleCrowdsCommon.AnimatedAgentPos > aAgentStates );

	// 
	//i32 beginFrame( 1: double aTime );
	//oneway void endFrame();
	//oneway void drawAgentInFrame( 1: i32 aFrameNumber, 
	//                                2: MultiscaleCrowdsCommon.AnimatedAgentPos aAgentState );
    //
}
